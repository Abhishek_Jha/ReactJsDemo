import React, { Component } from "react";
import ReactDOM from 'react-dom';
import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import "./Login.css";
import axios from 'axios';

    export default  class submitstudentMarks extends Component {
      constructor() {
        super();
        this.state = {
          math: 0,
          science: 0,
          english: 0,
          hindi:0,
          biology:0,
        };
      }

      onChange = (e) => {
        // Because we named the inputs to match their corresponding values in state, it's
        // super easy to update the state
        const state = this.state
        state[e.target.name] = e.target.value;
        this.setState(state);
      }

      onSubmit = (e) => {
        e.preventDefault();
        // get our form data out of state
        const { math, science,english, hindi, biology } = this.state;

        axios.post('/studentMarks', {  math, science,english, hindi, biology  })
          .then((result) => {
            //access the results here....
          });
          alert('Data has been submitted by student: ');
          window.location.reload(true);
      }

     

      render() {
        const { math, science,english, hindi, biology } = this.state;
        return (
          <form onSubmit={this.onSubmit}>
          <h1> Please Fill out all the Detail Asked by the form</h1>
            <label>
         Math Markss:
          
          </label>
            <input type="text" name="math" value={math} onChange={this.onChange} />
            <label>
         Science Marks:
          
          </label>
            <input type="text" name="science" value={science} onChange={this.onChange} />
            <label>
          English Marks:
          
          </label>
            <input type="text" name="english" value={english} onChange={this.onChange} />
            <label>
          Hindi Marks:
          
          </label>
            <input type="text" name="hindi" value={hindi} onChange={this.onChange} />
            <label>
          Biology Marks:
          
          </label>
            <input type="text" name="biology" value={biology} onChange={this.onChange} />
            
            <button type="submit">Submit</button>
          </form>
        );
      }
    }



ReactDOM.render(
  // <Game />,
  <submitMarks/>,
  document.getElementById('root')
);
