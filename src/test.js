import React, { Component } from "react";
import ReactDOM from 'react-dom';
import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import "./Login.css";
//var tableData;


export default class viewStudentMarks extends Component {
  constructor(props) {
    super(props);

    this.state = {
     tableData:[],
     query: '',
     sortBy:'_id'
    };
    // this.callApi = this.callApi.bind(this);
    // this.handleChange = this.handleChange.bind(this);
    // this.handleSubmit = this.handleSubmit.bind(this);
    // this.validateForm = this.validateForm.bind(this);
  }


  componentDidMount() {
    
     this.callApi()
      .then(res => {
        this.setState({ response : res })
      })
      .catch(err => console.log(err));
  }

  callApi = async () => {
    const response = await fetch('http://localhost:8000/getStudentMarks');
    const body = await response.json();
    console.log('bodyx  x =>',body);
    this.state.tableData = body;
    console.log('tableData=======>',this.state.tableData);

    if (response.status !== 200) throw Error(body.message);
    console.log('bodyx  x =>',body);
    return body;
  };



  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  handleSubmit = event => {
    event.preventDefault();
  }

   
    handleInputChange = () => {
      this.setState({
        query: this.search.value
      })
      console.log('query--->',this.search.value)
    }
   
  render() {
   
var items = this.state.tableData;

console.log('this.state.tableData=====',this.state.tableData)
       items = items.map(function (item) {
        return (
          
          <tr>
            <td>{item.id}</td>
            {/* <td>{item.email}</td> */}
            <td>{item.math}</td>
           <td>{item.science}</td>
            <td>{item.english}</td>
            <td>{item.hindi}</td>
            {/* <td>{parseInt(item.graduation)+parseInt(item.PUC)} </td> */}
            <td>{item.biology}</td>
            <td>{parseInt(item.math)+parseInt(item.science)+parseInt(item.english)+parseInt(item.hindi)+parseInt(item.biology)} </td>
            <td>{item.time}</td>
            </tr>    
        );
      });

      
      return (
        <div className="Student Details">
          <table>
            <tr>
            {/* <th>_id</th> */}
              <th>_id</th>
              <th>Math</th>
              <th>Science</th>
              <th>English</th>
              <th>Hindi</th>
              <th>Biology</th>
              <th>Total Marks</th>
              <th>Time</th>
            </tr>
            {items}
              </table>
              
          <div>
          <input
            placeholder="Search for..."
            ref={input => this.search = input}
            onChange={this.handleInputChange}
          />
          <p>{this.state.query}</p>
          </div>
  
        </div>
      );

      
  }

}

ReactDOM.render(
  // <Game />,
  <studentMarks />,
  document.getElementById('root')
);
