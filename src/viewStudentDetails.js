import React, { Component } from "react";
import ReactDOM from 'react-dom';
import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import "./Login.css";
//var tableData;


export default class viewStudentDetails extends Component {
  constructor(props) {
    super(props);

    this.state = {
     tableData:[],
     query: '',
     sortBy:'_id'
    };
    // this.callApi = this.callApi.bind(this);
    // this.handleChange = this.handleChange.bind(this);
    // this.handleSubmit = this.handleSubmit.bind(this);
    // this.validateForm = this.validateForm.bind(this);
  }


  componentDidMount() {
    
     this.callApi()
      .then(res => {
        this.setState({ response : res })
      })
      .catch(err => console.log(err));
  }

  callApi = async () => {
    const response = await fetch('http://localhost:8000/getStudent');
    const body = await response.json();
    console.log('bodyx  x =>',body);
    this.state.tableData = body;
    console.log('tableData=======>',this.state.tableData);

    if (response.status !== 200) throw Error(body.message);
    console.log('bodyx  x =>',body);
    return body;
  };



  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  handleSubmit = event => {
    event.preventDefault();
  }

   
    handleInputChange = () => {
      this.setState({
        query: this.search.value
      })
      console.log('query--->',this.search.value)
    }
   
  render() {
   
var items = this.state.tableData;

console.log('this.state.tableData=====',this.state.tableData)
       items = items.map(function (item) {
        return (
          
          <tr>
            <td>{item.id}</td>
            <td>{item.email}</td>
            <td>{item.name}</td>
           <td>{item.contactNo}</td>
            <td>{item.DOB}</td>
            <td>{item.address}</td>
            {/* <td>{parseInt(item.graduation)+parseInt(item.PUC)} </td> */}
            <td>{item.standard}</td>
            <td>{item.education}</td>
            <td>{item.time}</td>
          </tr>    
        );
      });

      
      return (
        <div className="Student Details">
          <table>
            <tr>
            <th>_id</th>
            <th>Student Email</th>
              <th>Student Name</th>
              <th>Contact Number</th>
              <th>DOB</th>
              <th>Address</th>
              <th>Standard</th>
              <th>Education</th>
              <th>Time</th>
            </tr>
            {items}
              </table>
              
          <div>
          <input
            placeholder="Search for..."
            ref={input => this.search = input}
            onChange={this.handleInputChange}
          />
          <p>{this.state.query}</p>
          </div>
  
        </div>
      );

      
  }

}

ReactDOM.render(
  // <Game />,
  <studentDetails />,
  document.getElementById('root')
);
