import React, { Component } from "react";
import ReactDOM from 'react-dom';
import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import "./Login.css";
import axios from 'axios';

    export default  class studentDetails extends Component {
      constructor() {
        super();
        this.state = {
          email: '',
          name: '',
          contactNo: '',
          DOB:Date.now(),
          address:'',
          standard:'',
          education:''
        };
      }

      onChange = (e) => {
        // Because we named the inputs to match their corresponding values in state, it's
        // super easy to update the state
        const state = this.state
        state[e.target.name] = e.target.value;
        this.setState(state);
      }

      onSubmit = (e) => {
        e.preventDefault();
        // get our form data out of state
        const { email, name,contactNo, DOB, address, standard, education  } = this.state;

        axios.post('/studentDetails', { email, name,contactNo, DOB, address, standard, education })
          .then((result) => {
            //access the results here....
          });
          alert('Data has been submitted by this name: ' + this.state.name);
          window.location.reload(true);
      }

     

      render() {
        const { email, name,contactNo, DOB, address, standard, education } = this.state;
        return (
          <form onSubmit={this.onSubmit}>
          <h1> Please Fill out all the Detail Asked by the form</h1>

            <label>
              Student email:
          </label>
            <input type="text" name="email" value={email} onChange={this.onChange} />

            <label>
         Student Name:
          </label>
            <input type="text" name="name" value={name} onChange={this.onChange} />

            <label>
            contactNo:
           </label>
            <input type="text" name="contactNo" value={contactNo} onChange={this.onChange} />
           
            <label>
            DOB:
           </label>
            <input type="text" name="DOB" value={DOB} onChange={this.onChange} />

            <label>
            address:
           </label>
            <input type="text" name="address" value={address} onChange={this.onChange} />

            <label>
            standard:
           </label>
            <input type="text" name="standard" value={standard} onChange={this.onChange} />

            <label>
            education:
           </label>
            <input type="text" name="education" value={education} onChange={this.onChange} />

            <button type="submit">Submit</button>
          </form>
        );
      }
    }



ReactDOM.render(
  // <Game />,
  <studentDetails/>,
  document.getElementById('root')
);
