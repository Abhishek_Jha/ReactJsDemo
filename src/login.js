import React, { Component } from "react";
import ReactDOM from 'react-dom';
import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import { withAlert } from 'react-alert'

import "./Login.css";
import studentDetails from './studentDetails';
import viewStudentDetails from './viewStudentDetails';
import viewStudentMarks from './test';
import submitstudentMarks from './search';
import axios from 'axios';



export default  class Login extends Component {
    constructor() {
      super();
      this.state = {
        username: '',
        password: ''
       
      };
      this.props = {
        student:'Student',
      };
    }
  
    onChange = (e) => {
      // Because we named the inputs to match their corresponding values in state, it's
      // super easy to update the state
      const state = this.state
      state[e.target.name] = e.target.value;
      this.setState(state);
    }
  
    onSubmit = (e) => {
      e.preventDefault();
      // get our form data out of state
      const { username, password } = this.state;
  
      axios.post('/login', { username, password})
        .then((result) => {
          //access the results here....
        });
       // alert('User SignedUp successfully: ' + this.props.student);
        window.location.reload(true);
    }
  
   
  
    render() {username
      const { username, password } = this.state;
      return (
        <form onSubmit={this.onSubmit}>
        <h1> Please Login here ...!</h1>
          <label>
       Student email:
        
        </label>
          <input type="email" name="username" value={username} onChange={this.onChange} />
          <label>
       Student password:
        
        </label>
          <input type="password" name="password" value={password} onChange={this.onChange} />
       
          
          <button type="submit">Submit</button>
        </form>
      );
    }
  }

  ReactDOM.render(
    <Login />,
   // <Signup/>,
   document.getElementById('root')
 );
 