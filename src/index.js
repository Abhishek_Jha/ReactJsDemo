import React, { Component } from "react";
import ReactDOM from 'react-dom';
import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import { withAlert } from 'react-alert'

import "./Login.css";
import studentDetails from './studentDetails';
import viewStudentDetails from './viewStudentDetails';
import viewStudentMarks from './test';
import Login from './login';
import submitstudentMarks from './search';
import axios from 'axios';



export default  class Signup extends Component {
  constructor() {
    super();
    this.state = {
      username: '',
      password: ''
     
    };
    this.props = {
      student:'Student',
    };
  }

  onChange = (e) => {
    // Because we named the inputs to match their corresponding values in state, it's
    // super easy to update the state
    const state = this.state
    state[e.target.name] = e.target.value;
    this.setState(state);
  }

  onSubmit = (e) => {
    e.preventDefault();
    // get our form data out of state
    const { username, password } = this.state;

    axios.post('/signup', { username, password})
      .then((result) => {
        //access the results here....
      });
     // alert('User SignedUp successfully: ' + this.props.student);
      window.location.reload(true);
  }

 

  render() {username
    const { username, password } = this.state;
    return (
      <form onSubmit={this.onSubmit}>
      <h1> Please SignUp here ...!</h1>
        <label>
     Student email:
      
      </label>
        <input type="email" name="username" value={username} onChange={this.onChange} />
        <label>
     Student password:
      
      </label>
        <input type="password" name="password" value={password} onChange={this.onChange} />
     
        
        <button type="submit">Submit</button>
      </form>
    );
  }
}



class App extends Component {
  render() {
     return (
        <Router>
           <div>
              <h2>Welcome to the example ...!</h2>
              <ul>
                 <li><Link to={'/'}>SignUp</Link></li>
                 <li><Link to={'/login'}>Login</Link></li>
                 <li><Link to={'/studentDetails'}>fillStudentDetails</Link></li>
                 <li><Link to={'/search'}>fillStudentMarks</Link></li>
                 <li><Link to={'/viewStudentDetails'}>viewStudentDetails</Link></li>
                 <li><Link to={'/viewStudentMarks'}>viewStudentMarks</Link></li>
                 
              </ul>
              <hr />
              
              <Switch>
                 <Route exact path='/' component={Signup} />
                 <Route exact path='/login' component={Login} />
                 <Route exact path='/studentDetails' component={studentDetails} />
                 <Route exact path='/viewStudentDetails' component={viewStudentDetails} />
                 <Route exact path='/viewStudentMarks' component={viewStudentMarks} />
                 <Route exact path='/search' component={submitstudentMarks} />
                
              </Switch>
           </div> 
        </Router>
     );
    }
  } 

ReactDOM.render(
   <App />,
  // <Signup/>,
  document.getElementById('root')
);
